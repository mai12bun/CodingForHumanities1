## My expectations for "Coding For Humanities"

I hope to aquire proper skills in coding, as I have none at all. So far im enjoying it, so I'm quite optimistic.

#### Furthermore I look forward to:

* Getting to know my fellow students
* Working on a group project

On slightly different note, in honor of his Nobel prize win, [here](https://www.youtube.com/watch?v=pjYvfvpiJSs) is a live version of my favourite Bob Dylan song. 
