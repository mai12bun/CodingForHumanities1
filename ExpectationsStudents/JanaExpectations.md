## First Assignment

Hi I'm Jana and I'm studying [Digital Humanities](https://www.leipzig-studieren.de/digital-humanities-bsc/). I'm living in [Leipzig](https://de.wikipedia.org/wiki/Leipzig) for 7 years now and I love it.

### My Expectations about Digital Humanities 

* to make new experiences 
* to learn more about linguistic data processing
* to work hand in hand with people of different departments on interesting projects

